<?php

namespace Database\Seeders;

use App\Models\Relations\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        City::create([
            'city_name' => 'Lahore'
        ]);

        //2
        City::create([
            'city_name'=>"Karachi"
        ]);

    }
}
