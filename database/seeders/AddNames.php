<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Seeder;

class AddNames extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = "Saqib";

        Profile::create([
            'name' => $name
        ]);

        Profile::create([
          'name' =>  'Hamza'
        ]);

        Profile::create([
           'name' =>'Aman' 
        ]);


        Profile::create(
            [
                'name' => "Hassan"
            ]
        );

        Profile::create([
            'name' => "Muaaz"
        ]);
    }
}
