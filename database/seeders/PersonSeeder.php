<?php

namespace Database\Seeders;

use App\Models\Relations\Person;
use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Person::create([
            'city_id' => 1,
            'name' => "Hassan"
        ]);

        Person::create([
            'city_id' => 1,
            'name' => "Sufyan"
        ]);

        Person::create([
            'city_id' => 1,
            'name' => 'Hamza'
        ]);
        Person::create([
            'name' => "Saqib",
            'city_id' => 2
        ]);
    }
}
