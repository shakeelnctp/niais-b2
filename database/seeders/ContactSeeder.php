<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'profile_id' => 1,
            'contact_number' => "030023234234234"
        ]);
        Contact::create([
            'profile_id' => 2,
            'contact_number' => "03002311111114"
        ]);
        Contact::create([
            'profile_id' => 3,
            'contact_number' => "03002356756"
        ]);
        Contact::create([
            'profile_id' => 4,
            'contact_number' => "03002356754566"
        ]);
        
        
    }
}
