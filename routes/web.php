<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

////////Return View by only URl

// Route::get('my-profile',function(){
// return view('profile.myProfile');
// }
// );

// Route

////// Return View with Controller 


// Route::get('/my-profile',[IndexController::class,'myProfile']);


///////middleware in route //////////////////////////
Route::get('create-profile',[IndexController::class,'createProfile'])->name('create')->middleware('auth');
Route::post('store-profile',[IndexController::class,'storeProfile'])->name('store')->middleware('auth');


Route::get('my-profile/',[IndexController::class,'myProfile'])->name('profile')->middleware('auth');

///////////////////////////////////////////////////////
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
