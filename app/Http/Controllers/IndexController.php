<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{

  ///////Middleware n in Construtor////////

public function createProfile()
{
  $user =  Auth::user();
  if($user->profile)
  {
    return redirect()->route('profile');
  }
  return view('profile.createProfile');
}

public function storeProfile(Request $request)
{

  $request->validate([
    'my_cnic' => "required",
    'my_mobile' => "required",
    'my_address' => "required",
  ]);
  $user =  Auth::user();
 
  // dd($user->id);
  Profile::create([
'user_id' => $user->id, 
'cnic' => $request->my_cnic, 
'mobile' => $request->my_mobile,
'address' => $request->my_address
  ]);

  return redirect()->route('profile');

}
 public function myProfile()
 {
    ///////Middleware in Function////////
    // dd($c);
     $user = Auth::user();
     if(!$user->profile)
     {
      return redirect()->route('create');
     }
      // dd($user);
    return view('profile.myProfile',compact('user'));
  

  
 }
  

}
