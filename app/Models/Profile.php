<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    
    use HasFactory;
  protected $fillable = ['cnic','user_id','mobile','address'];
    public function contact()
    {
        return $this->hasOne(Contact::class,'profile_id','id');
    }
}
