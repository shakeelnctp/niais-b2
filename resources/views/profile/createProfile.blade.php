<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Profle</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form  action="{{route('store')}}" method="POST" class="container m-5 border">
        @csrf
        <div class="form-group">
          <label for="exampleInputEmail1">CNIC *</label>
          <input type="text" class="form-control" placeholder="Please Enter Your CNIC" maxlength="13" minlength="13" name="my_cnic" id="exampleInputEmail1" aria-describedby="emailHelp" required>
         
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Mobile</label>
          <input type="number" class="form-control" name="my_mobile" placeholder="Please Enter Your Mobile Number" minlength="11" maxlength="11" id="exampleInputPassword1" required>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Address</label>
            <input type="text" class="form-control" name="my_address" placeholder="Please Enter Your Address" minlength="5" maxlength="500" id="exampleInputPassword1" required>
          </div>
         
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</body>
</html>